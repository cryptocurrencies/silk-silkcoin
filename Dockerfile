# ---- Base Node ---- #
FROM ubuntu:16.04 AS Base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev libqrencode-dev git-core libleveldb-dev libevent-dev libboost-all-dev libdb++-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/silkcointemple/silkcoin /opt/silkcoin && \
    cd /opt/silkcoin/src && \
    cd /opt/silkcoin/src/leveldb/ && make libleveldb.a libmemenv.a && cd .. && \
    make -f makefile.unix

# ---- Release ---- #
FROM ubuntu:16.04 as release 
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y  libboost-all-dev libdb5.3-dev libdb5.3++-dev libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r silkcoin && useradd -r -m -g silkcoin silkcoin
RUN mkdir /data
RUN chown silkcoin:silkcoin /data
COPY --from=build /opt/silkcoin/src/silkcoind /usr/local/bin/
USER silkcoin
VOLUME /data
EXPOSE 16666 16667
CMD ["/usr/local/bin/silkcoind", "-datadir=/data", "-conf=/data/silkcoin.conf", "-server", "-txindex", "-printtoconsole", "-reindex"]